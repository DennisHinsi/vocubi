/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import dagger.Module
import dennis.hinsi.domain.model.Book
import dennis.hinsi.domain.repository.BookRepository


@Module
class BookProviderUC(
    private val bookRepository: BookRepository
) {
    fun observe(): LiveData<List<Book>> = bookRepository.observe()

    fun observeAllNotEmpty(): LiveData<List<Book>> =
        Transformations.map(bookRepository.observeExtended()) { list ->
            list.filter { bookExt -> bookExt.vocList.isNotEmpty() }
                .map { bookExt -> bookExt.book }
        }

    fun load(id: String) = bookRepository.load(id)

    fun search(query: String) = bookRepository.search(query)
}
