/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecase

import androidx.lifecycle.LiveData
import dennis.hinsi.domain.model.TrainShowResult
import dennis.hinsi.domain.model.TrainShowResultExtended
import dennis.hinsi.domain.repository.TrainShowResultRepository
import javax.inject.Inject

class TrainShowResultProviderUC @Inject constructor(
    private val trainShowRepository: TrainShowResultRepository
){
     suspend fun insert(trainShow: TrainShowResult) = trainShowRepository.insert(trainShow)

     suspend fun update(trainShow: TrainShowResult) = trainShowRepository.update(trainShow)

     fun observe(
          trainId: String
     ): LiveData<List<TrainShowResultExtended>> = trainShowRepository.observe(trainId)

     suspend fun load(id: String): TrainShowResult = trainShowRepository.load(id)

     suspend fun loadAll(
          trainId: String,
          bookId: String
     ) = trainShowRepository.loadAll(trainId, bookId)
}