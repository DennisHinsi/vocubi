/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecase

import dagger.Module
import dennis.hinsi.domain.model.TrainWrite
import dennis.hinsi.domain.repository.TrainWriteRepository

@Module
class TrainWriteEditorUC(private val trainWriteRepository: TrainWriteRepository) {
    suspend fun insert(trainWrite: TrainWrite) = trainWriteRepository.insert(trainWrite)

    suspend fun update(trainWrite: TrainWrite) = trainWriteRepository.update(trainWrite)

    suspend fun delete(id: String) = trainWriteRepository.delete(id)

    suspend fun deleteAll(bookId: String) = trainWriteRepository.deleteAll(bookId)
}

