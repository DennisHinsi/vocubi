/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecase

import dagger.Module
import dennis.hinsi.domain.model.Book
import dennis.hinsi.domain.repository.BookRepository

@Module
class BookEditorUC(
    private val bookRepository: BookRepository,
    private val vocEditorUC: VocEditorUC,
    private val trainShowEditorUC: TrainShowEditorUC,
    private val trainShowResultEditorUC: TrainShowResultEditorUC
) {
    suspend fun create(book: Book) {
        bookRepository.create(book)
    }

    suspend fun update(
        bookId: String,
        newTitle: String,
        lang0: String,
        lang1: String,
        lang0Accent: Int,
        lang1Accent: Int
    ) {
        bookRepository.update(
            bookRepository.loadSingle(bookId).apply {
                modified = System.currentTimeMillis()
                title = newTitle
                language_name_0 = lang0
                language_name_1 = lang1
                accent_0 = lang0Accent
                accent_1 = lang1Accent
            }
        )
    }

    suspend fun delete(bookId: String) {
        bookRepository.delete(bookId)
        vocEditorUC.deleteAllOfBook(bookId)
        trainShowEditorUC.deleteAllOfBook(bookId)
        trainShowResultEditorUC.deleteAllOfBook(bookId)
    }
}