/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecase

import dagger.Module
import dennis.hinsi.domain.model.Voc
import dennis.hinsi.domain.repository.VocRepository

@Module
class VocEditorUC(
    private val vocProviderUC: VocProviderUC,
    private val vocRepository: VocRepository
) {
    suspend fun create(voc: Voc) = vocRepository.create(voc)

    suspend fun update(
        vocId: String,
        exp0: String,
        exp1: String
    ) = vocRepository.update(vocProviderUC.load(vocId).apply {
        this.expressionLang0 = exp0
        this.expressionLang1 = exp1
    })

    suspend fun delete(id: String) = vocRepository.delete(id)
    
    suspend fun deleteAllOfBook(bookId: String) = vocRepository.deleteAllOfBook(bookId)
}