/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import dagger.Module
import dennis.hinsi.domain.model.Book
import dennis.hinsi.domain.model.TrainWrite
import dennis.hinsi.domain.model.TrainWriteResult
import dennis.hinsi.domain.model.Voc
import dennis.hinsi.domain.repository.TrainWriteRepository

@Module
class TrainWriteProviderUC(private val trainWriteRepository: TrainWriteRepository) {

    suspend fun load(id: String): TrainWrite = with(trainWriteRepository.load(id)) {
        trainWrite.merge(book, vocList, trainResultList)
    }

    fun observe(): LiveData<List<TrainWrite>> =
        Transformations.map(trainWriteRepository.observe()) { list ->
            list.map { trainWriteExt ->
                trainWriteExt.trainWrite.merge(
                    trainWriteExt.book,
                    trainWriteExt.vocList,
                    trainWriteExt.trainResultList
                )
            }
        }

    private fun TrainWrite.merge(
        book: Book,
        vocList: List<Voc>,
        trainResultList: List<TrainWriteResult>
    ): TrainWrite = apply {
        title = book.title
        maxCount = vocList.size
        progress = trainResultList.size
        correctCount = trainResultList.count { it.isResultCorrect }
        wrongCount = trainResultList.count { !it.isResultCorrect }
    }
}