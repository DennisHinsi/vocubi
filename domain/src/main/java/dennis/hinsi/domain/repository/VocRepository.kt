/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.domain.model.Voc

interface VocRepository {
    suspend fun create(voc: Voc)
    suspend fun update(voc: Voc)
    fun observe(bookId: String): LiveData<List<Voc>>
    suspend fun loadAllSingle(bookId: String): List<Voc>
    fun search(query: String): LiveData<List<Voc>>
    suspend fun delete(id: String)
    suspend fun deleteAllOfBook(bookId: String)
    suspend fun load(id: String): Voc
}