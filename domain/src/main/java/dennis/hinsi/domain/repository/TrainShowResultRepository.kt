/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.domain.model.TrainShowResult
import dennis.hinsi.domain.model.TrainShowResultExtended

interface TrainShowResultRepository {
    suspend fun insert(trainShowResult: TrainShowResult)
    suspend fun update(trainShowResult: TrainShowResult)
    suspend fun delete(id: String)
    suspend fun deleteAllOfBook(bookId: String)
    fun observe(trainId: String): LiveData<List<TrainShowResultExtended>>
    suspend fun load(id: String): TrainShowResult
    suspend fun loadAll(id: String, bookId: String): List<TrainShowResult>
}