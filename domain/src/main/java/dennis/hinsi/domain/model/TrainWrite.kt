/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.model

import androidx.room.*

@Entity(tableName = "train_write")
data class TrainWrite(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,

    @ColumnInfo(name = "book_id")
    val bookId: String,

    @ColumnInfo(name = "created")
    val created: Long
) {
    @Ignore
    var title: String = ""

    @Ignore
    var maxCount: Int = 0

    @Ignore
    var progress: Int = 0

    @Ignore
    var correctCount: Int = 0

    @Ignore
    var wrongCount: Int = 0
}

data class TrainWriteExtended(
    @Embedded
    val trainWrite: TrainWrite,

    @Relation(
        parentColumn = "book_id",
        entityColumn = "id"
    )
    val book: Book,

    @Relation(
        parentColumn = "book_id",
        entityColumn = "book_id"
    )
    val vocList: List<Voc>,

    @Relation(
        parentColumn = "id",
        entityColumn = "train_id"
    )
    val trainResultList: List<TrainWriteResult>
)
