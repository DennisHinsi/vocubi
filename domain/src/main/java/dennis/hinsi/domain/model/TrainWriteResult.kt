/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.model

import androidx.room.*

@Entity(tableName = "train_write_result")
data class TrainWriteResult(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,

    @ColumnInfo(name = "train_id")
    val trainId: String,

    @ColumnInfo(name = "book_id")
    val bookId: String,

    @ColumnInfo(name = "voc_id")
    val vocId: String,

    @ColumnInfo(name = "result")
    val result: String,

    @ColumnInfo(name = "is_result_correct")
    var isResultCorrect: Boolean
)

data class TrainWriteResultExtended(
    @Embedded
    val trainWriteResult: TrainWriteResult,

    @Relation(
        parentColumn = "voc_id",
        entityColumn = "id"
    )
    val voc: Voc
)