/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.model

import androidx.room.*

@Entity(tableName = "train_show_result")
data class TrainShowResult(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,

    @ColumnInfo(name = "time")
    var time: Long,

    @ColumnInfo(name = "voc_id")
    val vocId: String,

    @ColumnInfo(name = "book_id")
    val bookId: String,

    @ColumnInfo(name = "train_id")
    val trainId: String,

    @ColumnInfo(name = "result")
    var isResultCorrect: Boolean
)

data class TrainShowResultExtended(
    @Embedded
    val trainShowResult: TrainShowResult,

    @Relation(
        parentColumn = "voc_id",
        entityColumn = "id"
    )
    val voc: Voc
)