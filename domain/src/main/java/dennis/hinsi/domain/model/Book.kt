/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.model

import androidx.room.*

@Entity(tableName = "BookContent")
data class Book(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,

    @ColumnInfo(name = "title")
    var title: String,

    @ColumnInfo(name = "created")
    val created: Long,

    @ColumnInfo(name = "modified")
    var modified: Long,

    @ColumnInfo(name = "language_name_0")
    var language_name_0: String,

    @ColumnInfo(name = "language_name_1")
    var language_name_1: String,

    @ColumnInfo(name = "accent_0")
    var accent_0: Int,

    @ColumnInfo(name = "accent_1")
    var accent_1: Int
) {
    @Ignore
    var isEmpty: Boolean = true
}

data class BookExtended(
    @Embedded
    val book: Book,

    @Relation(
        parentColumn = "id",
        entityColumn = "book_id"
    )
    val vocList: List<Voc>
)
