/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi

import android.app.Application
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject
import dagger.android.support.HasSupportFragmentInjector
import dennis.hinsi.vocubi.dagger.*
import dennis.hinsi.vocubi.dagger.module.AppModule
import dennis.hinsi.vocubi.dagger.module.RepositoryModule
import dennis.hinsi.vocubi.dagger.module.RoomModule
import dennis.hinsi.vocubi.dagger.module.UseCaseModule

class VocubiApplication: Application(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        DaggerVocubiComponent.builder()
            .application(this)
            .appModule(AppModule(this))
            .repositoryModule(RepositoryModule())
            .useCaseModule(UseCaseModule())
            .roomModule(RoomModule(this))
            .build()
            .inject(this)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

}