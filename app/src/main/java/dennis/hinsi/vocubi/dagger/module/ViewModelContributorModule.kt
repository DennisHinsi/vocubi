/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.dagger.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dennis.hinsi.vocubi.dagger.vm.ViewModelFactory
import dennis.hinsi.vocubi.dagger.vm.ViewModelKey
import dennis.hinsi.vocubi.ui.main.viewmodels.BookVM
import dennis.hinsi.vocubi.ui.main.viewmodels.OverviewVM
import dennis.hinsi.vocubi.ui.main.viewmodels.SearchVM
import dennis.hinsi.vocubi.ui.main.viewmodels.VocVM
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainOverViewVM
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainShowVM
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainWriteVM

@Module
abstract class ViewModelContributorModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(OverviewVM::class)
    internal abstract fun provideOverviewVM(viewModel: OverviewVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookVM::class)
    internal abstract fun provideBookVM(viewModel: BookVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VocVM::class)
    internal abstract fun provideVocVM(viewModel: VocVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchVM::class)
    internal abstract fun provideSearchVM(viewModel: SearchVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrainOverViewVM::class)
    internal abstract fun provideTrainOverviewViewModelVM(viewModel: TrainOverViewVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrainShowVM::class)
    internal abstract fun provideTrainShowViewModelVM(viewModel: TrainShowVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrainWriteVM::class)
    internal abstract fun provideTrainWriteViewModelVM(viewModel: TrainWriteVM): ViewModel
}

