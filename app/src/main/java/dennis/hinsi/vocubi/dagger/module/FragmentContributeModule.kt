/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.dagger.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dennis.hinsi.vocubi.dagger.FragmentScope
import dennis.hinsi.vocubi.ui.main.fragments.BookFragment
import dennis.hinsi.vocubi.ui.main.fragments.CreateBookFragment
import dennis.hinsi.vocubi.ui.main.fragments.EditBookFragment
import dennis.hinsi.vocubi.ui.main.fragments.OverviewFragment
import dennis.hinsi.vocubi.ui.main.fragments.SearchFragment
import dennis.hinsi.vocubi.ui.main.fragments.CreateVocFragment
import dennis.hinsi.vocubi.ui.main.fragments.EditVocFragment
import dennis.hinsi.vocubi.ui.train.fragments.*

@Module
abstract class FragmentContributeModule {

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeOverviewFragment(): OverviewFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeCreateBookFragment(): CreateBookFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeBookFragment(): BookFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeEditBookFragment(): EditBookFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeCreateVocFragment(): CreateVocFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeEditVocFragment(): EditVocFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeSearchFragment(): SearchFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeTrainOverviewFragment(): TrainOverviewFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeTrainBookSelectorFragment(): TrainBookSelectorFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeTrainShowFragment(): TrainShowFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeTrainShowResultFragment(): TrainResultFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeTrainWriteFragment(): TrainWriteFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeTrainResultFragment(): TrainResultPageFragment?
}
