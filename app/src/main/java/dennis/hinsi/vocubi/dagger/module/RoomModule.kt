/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.dagger.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dennis.hinsi.data.database.AppDatabase
import javax.inject.Singleton

@Module
class RoomModule(context: Context) {

    private val db = Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        "content.db"
    ).build()

    @Provides
    @Singleton
    fun provideBookDAO() = db.bookDao()

    @Provides
    @Singleton
    fun provideVocDAO() = db.vocDao()

    @Provides
    @Singleton
    fun provideTrainShowDAO() = db.trainShow()

    @Provides
    @Singleton
    fun provideTrainResultDAO() = db.trainShowResult()

    @Provides
    @Singleton
    fun provideTrainWriteDAO() = db.trainWrite()

    @Provides
    @Singleton
    fun provideTrainWriteResultDAO() = db.trainWriteResult()
}