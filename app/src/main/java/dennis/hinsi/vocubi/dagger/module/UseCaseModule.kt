/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.dagger.module

import dagger.Module
import dagger.Provides
import dennis.hinsi.domain.repository.*
import dennis.hinsi.domain.usecase.*
import javax.inject.Singleton

@Module
class UseCaseModule {
    @Provides
    @Singleton
    fun provideBookEditorUC(
        bookRepository: BookRepository,
        vocEditorUC: VocEditorUC,
        trainShowEditorUC: TrainShowEditorUC,
        trainShowResultEditorUC: TrainShowResultEditorUC
    ) = BookEditorUC(bookRepository, vocEditorUC, trainShowEditorUC, trainShowResultEditorUC)

    @Provides
    @Singleton
    fun provideBookProviderUC(bookRepository: BookRepository) = BookProviderUC(bookRepository)

    @Provides
    @Singleton
    fun provideVocEditorUC(
        vocProviderUC: VocProviderUC,
        vocRepository: VocRepository
    ) = VocEditorUC(vocProviderUC, vocRepository)

    @Provides
    @Singleton
    fun provideVocProviderUC(vocRepository: VocRepository) = VocProviderUC(vocRepository)

    @Provides
    @Singleton
    fun provideTrainShowProviderUC(
        trainShowRepository: TrainShowRepository
    ) = TrainShowProviderUC(
        trainShowRepository
    )

    @Provides
    @Singleton
    fun provideTrainShowEditorUC(trainShowRepository: TrainShowRepository) =
        TrainShowEditorUC(trainShowRepository)

    @Provides
    @Singleton
    fun provideTrainShowResultProviderUC(trainShowRepository: TrainShowResultRepository) =
        TrainShowResultProviderUC(trainShowRepository)

    @Provides
    @Singleton
    fun provideTrainShowResultEditorUC(trainShowRepository: TrainShowResultRepository) =
        TrainShowResultEditorUC(trainShowRepository)

    @Provides
    @Singleton
    fun provideTrainWriteEditorUC(trainWriteRepository: TrainWriteRepository) =
        TrainWriteEditorUC(trainWriteRepository)

    @Provides
    @Singleton
    fun provideTrainWriteProviderUC(trainWriteRepository: TrainWriteRepository) =
        TrainWriteProviderUC(trainWriteRepository)

    @Provides
    @Singleton
    fun provideTrainWriteResultEditorUC(trainWriteRepository: TrainWriteResultRepository) =
        TrainWriteResultEditorUC(trainWriteRepository)

    @Provides
    @Singleton
    fun provideTrainWriteResultProviderUC(trainWriteRepository: TrainWriteResultRepository) =
        TrainWriteResultProviderUC(trainWriteRepository)
}