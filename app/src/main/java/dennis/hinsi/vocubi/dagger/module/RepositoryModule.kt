/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.dagger.module

import dagger.Module
import dagger.Provides
import dennis.hinsi.data.database.*
import dennis.hinsi.data.repository.*
import dennis.hinsi.domain.repository.*
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideBookRepository(bookDAO: BookDAO): BookRepository = BookRepositoryImpl(bookDAO)

    @Provides
    @Singleton
    fun provideVocRepository(vocDAO: VocDAO): VocRepository = VocRepositoryImpl(vocDAO)

    @Provides
    @Singleton
    fun provideTrainShowRepository(
        trainShowDAO: TrainShowDAO
    ): TrainShowRepository = TrainShowRepositoryImpl(trainShowDAO)

    @Provides
    @Singleton
    fun provideTrainShowResultRepository(
        trainShowResultDAO: TrainShowResultDAO
    ): TrainShowResultRepository = TrainShowResultRepositoryImpl(trainShowResultDAO)

    @Provides
    @Singleton
    fun provideTrainWriteRepository(
        trainShowResultDAO: TrainWriteDAO
    ): TrainWriteRepository = TrainWriteRepositoryImpl(trainShowResultDAO)

    @Provides
    @Singleton
    fun provideTrainWriteResultRepository(
        trainShowResultDAO: TrainWriteResultDAO
    ): TrainWriteResultRepository = TrainWriteResultRepositoryImpl(trainShowResultDAO)
}