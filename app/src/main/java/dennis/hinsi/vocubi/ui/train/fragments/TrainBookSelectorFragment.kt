/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.train.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseFragment
import dennis.hinsi.vocubi.ui.adapter.BookAdapter
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainOverViewVM
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.frg_train_selector.*
import javax.inject.Inject

class TrainBookSelectorFragment : BaseFragment() {

    @Inject
    lateinit var trainOverViewVM: TrainOverViewVM

    private lateinit var adapter: BookAdapter

    private val args: TrainBookSelectorFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.frg_train_selector, container, false
    )

    override fun prepareUI() {
        adapter = BookAdapter()
        view_list.layoutManager = LinearLayoutManager(requireContext())
        view_list.adapter = adapter

    }

    override fun prepareOnClickListeners() {
        toolbar_back.setOnClickListener {
            findNavController().popBackStack()
        }

        adapter.clickStream
            .doOnError(::rxStreamError)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { id ->
                when (args.TRAINMETHOD) {
                    ATTR_TRAIN_METHOD_VIEW -> {
                        findNavController().navigate(
                            TrainBookSelectorFragmentDirections
                                .actionTrainBookSelectorFragmentToTrainShowFragment(id)
                        )
                    }
                    ATTR_TRAIN_METHOD_WRITE -> {
                        findNavController().navigate(
                            TrainBookSelectorFragmentDirections
                                .actionTrainBookSelectorFragmentToTrainWriteFragment(id)
                        )
                    }
                }
            }
            .addToSubscriptions()
    }

    override fun observeData() {
        trainOverViewVM.observeBooks().observe(viewLifecycleOwner, Observer {
            adapter.list = it
        })
    }

    companion object {
        const val ATTR_TRAIN_METHOD_VIEW = "train_view"
        const val ATTR_TRAIN_METHOD_WRITE = "train_write"
    }
}