/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.train.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseFragment
import dennis.hinsi.vocubi.ui.adapter.ResultPagerAdapter
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainOverViewVM
import kotlinx.android.synthetic.main.frg_train_overview.*
import javax.inject.Inject

class TrainOverviewFragment : BaseFragment() {

    @Inject
    lateinit var trainOverViewVM: TrainOverViewVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.frg_train_overview, container, false
    )

    override fun prepareUI() = Unit

    override fun prepareOnClickListeners() {
        toolbar_back.setOnClickListener {
            requireActivity().finish()
        }
        bottom_bar_train_show.setOnClickListener {
            findNavController().navigate(
                TrainOverviewFragmentDirections
                    .actionTrainOverviewFragmentToTrainBookSelectorFragment(
                        TrainBookSelectorFragment.ATTR_TRAIN_METHOD_VIEW
                    )
            )
        }
        bottom_bar_train_write.setOnClickListener {
            findNavController().navigate(
                TrainOverviewFragmentDirections
                    .actionTrainOverviewFragmentToTrainBookSelectorFragment(
                        TrainBookSelectorFragment.ATTR_TRAIN_METHOD_WRITE
                    )
            )
        }
    }

    override fun observeData() {
        tab_layout.setupWithViewPager(view_pager)
        view_pager.adapter = ResultPagerAdapter(resources, childFragmentManager)
    }

    companion object {
        const val METHOD_SHOW = 0
        const val METHOD_WRITE = 1
    }
}
