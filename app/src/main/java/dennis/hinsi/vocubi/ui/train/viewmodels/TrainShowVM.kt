/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.train.viewmodels

import androidx.lifecycle.liveData
import dennis.hinsi.domain.model.TrainShow
import dennis.hinsi.domain.model.TrainShowResult
import dennis.hinsi.domain.usecase.*
import dennis.hinsi.vocubi.base.CoroutineViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class TrainShowVM @Inject constructor(
    private val bookProviderUC: BookProviderUC,
    private val vocProviderUC: VocProviderUC,
    private val trainShowEditorUC: TrainShowEditorUC,
    private val trainShowResultEditorUC: TrainShowResultEditorUC,
    private val trainShowResultProviderUC: TrainShowResultProviderUC
) : CoroutineViewModel() {
    fun loadBook(bookId: String) = liveData(Dispatchers.IO) {
        emitSource(bookProviderUC.load(bookId))
    }

    fun loadVocList(bookId: String) = liveData(Dispatchers.IO) {
        emit(vocProviderUC.loadAllSingle(bookId))
    }

    fun insert(trainShow: TrainShow) = backgroundScope.launch(coroutineExceptions) {
        trainShowEditorUC.insert(trainShow)
    }

    fun insert(trainShowResult: TrainShowResult) = backgroundScope.launch(coroutineExceptions) {
        trainShowResultEditorUC.insert(trainShowResult)
    }

    fun observeTrainShowResult(trainId: String) = trainShowResultProviderUC.observe(trainId)

    fun delete(trainId: String) = backgroundScope.launch(coroutineExceptions) {
        trainShowEditorUC.delete(trainId)
        trainShowResultEditorUC.delete(trainId)
    }
}