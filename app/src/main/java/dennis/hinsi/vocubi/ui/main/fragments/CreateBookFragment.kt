/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding4.view.clicks
import dennis.hinsi.domain.model.Book
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseSheetDialogFragment
import dennis.hinsi.vocubi.ui.main.viewmodels.BookVM
import dennis.hinsi.vocubi.util.loadAccentColors
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.dialog_create_book.*
import java.util.*
import javax.inject.Inject

class CreateBookFragment : BaseSheetDialogFragment() {

    @Inject
    lateinit var bookVM: BookVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_create_book, container, false)

    override fun prepareUI() {
        create_book_delete.visibility = View.GONE
        requireContext().loadAccentColors().let { list ->
            color_picker_view_0.setColors(list)
            color_picker_view_1.setColors(list)
        }
    }

    override fun prepareOnClickListeners() {
        create_book_cancel.clicks()
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { dismiss() }
            .addToSubscriptions()

        create_book_submit.clicks()
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                val timestamp = System.currentTimeMillis()
                bookVM.create(
                    Book(
                        id = UUID.randomUUID().toString(),
                        created = timestamp,
                        modified = timestamp,
                        title = create_book_title.text.toString(),
                        language_name_0 = create_book_lang_0_name.text.toString(),
                        language_name_1 = create_book_lang_1_name.text.toString(),
                        accent_0 = color_picker_view_0.selectedColor,
                        accent_1 = color_picker_view_1.selectedColor
                    )
                )
                dismiss()
            }
            .addToSubscriptions()
    }

    override fun observeData() = Unit
}

