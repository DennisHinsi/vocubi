/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.domain.model.TrainShow
import dennis.hinsi.domain.model.TrainWrite
import dennis.hinsi.vocubi.ui.exceptions.UnknownViewHolderException
import dennis.hinsi.vocubi.ui.viewholder.TrainOverviewShowVH
import dennis.hinsi.vocubi.ui.viewholder.TrainOverviewWriteVH
import dennis.hinsi.vocubi.ui.train.fragments.TrainOverviewFragment.Companion.METHOD_SHOW
import dennis.hinsi.vocubi.ui.train.fragments.TrainOverviewFragment.Companion.METHOD_WRITE
import io.reactivex.rxjava3.subjects.PublishSubject

class TrainOverviewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list = mutableListOf<Any>()
    var clickStream: PublishSubject<Pair<Int, String>> = PublishSubject.create<Pair<Int, String>>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder = when (viewType) {
        METHOD_SHOW -> TrainOverviewShowVH.newInstance(parent)
        METHOD_WRITE -> TrainOverviewWriteVH.newInstance(parent)
        else -> throw UnknownViewHolderException()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TrainOverviewShowVH -> holder.bind(
                item = list[position] as TrainShow,
                isLast = position + 1 == list.size,
                clickStream = clickStream
            )

            is TrainOverviewWriteVH -> holder.bind(
                item = list[position] as TrainWrite,
                isLast = position + 1 == list.size,
                clickStream = clickStream
            )
        }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int = when (list[position]) {
        is TrainShow -> METHOD_SHOW
        is TrainWrite -> METHOD_WRITE
        else -> throw UnknownViewHolderException()
    }

    fun setList(list: List<Any>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

}



