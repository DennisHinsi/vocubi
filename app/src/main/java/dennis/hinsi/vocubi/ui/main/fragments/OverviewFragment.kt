/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.main.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding4.view.clicks
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseFragment
import dennis.hinsi.vocubi.ui.adapter.BookAdapter
import dennis.hinsi.vocubi.ui.main.viewmodels.OverviewVM
import dennis.hinsi.vocubi.ui.settings.SettingsActivity
import dennis.hinsi.vocubi.ui.train.TrainActivity
import dennis.hinsi.vocubi.util.AttrUtil
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.frg_main_overview.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class OverviewFragment : BaseFragment() {

    @Inject
    lateinit var overviewVM: OverviewVM

    private lateinit var adapter: BookAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_main_overview, container, false)

    override fun prepareUI() {
        adapter = BookAdapter()
        view_list.layoutManager = LinearLayoutManager(requireContext())
        view_list.adapter = adapter
    }

    override fun prepareOnClickListeners() {
        bottom_bar_add_book.clicks()
            .debounce(AttrUtil.VIEW_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .doOnError(::rxStreamError)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                findNavController().navigate(
                    OverviewFragmentDirections.actionOverviewFragmentToCreateBookFragment()
                )
            }
            .addToSubscriptions()

        bottom_bar_train.clicks()
            .debounce(AttrUtil.VIEW_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .doOnError(::rxStreamError)
            .subscribe { startActivity(Intent(requireActivity(), TrainActivity::class.java)) }
            .addToSubscriptions()

        bottom_bar_settings.clicks()
            .debounce(AttrUtil.VIEW_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .doOnError(::rxStreamError)
            .subscribe { startActivity(Intent(requireContext(), SettingsActivity::class.java)) }
            .addToSubscriptions()

        toolbar_search.clicks()
            .debounce(AttrUtil.VIEW_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .doOnError(::rxStreamError)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                findNavController().navigate(
                    OverviewFragmentDirections.actionOverviewFragmentToSearchFragment()
                )
            }
            .addToSubscriptions()

        adapter.clickStream
            .debounce(AttrUtil.ADAPTER_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .doOnError(::rxStreamError)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { bookId ->
                findNavController().navigate(
                    OverviewFragmentDirections.actionOverviewFragmentToBookFragment(bookId)
                )
            }
            .addToSubscriptions()
    }

    override fun observeData() {
        overviewVM.observe().observe(viewLifecycleOwner, Observer { bookList ->
            adapter.list = bookList
        })
    }
}