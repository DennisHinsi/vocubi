/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.train.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.AndroidSupportInjection
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.ui.adapter.TrainResultAdapter
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainShowVM
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainWriteVM
import dennis.hinsi.vocubi.util.requireNonNull
import kotlinx.android.synthetic.main.frg_train_show_result.*
import javax.inject.Inject

class TrainResultFragment : Fragment() {

    @Inject
    lateinit var trainShowVM: TrainShowVM

    @Inject
    lateinit var trainWriteVM: TrainWriteVM

    private var trainMode: Int = -1
    private lateinit var trainId: String
    private lateinit var adapter: TrainResultAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.frg_train_show_result, container, false
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        trainMode = requireArguments().getInt(ATTR_TRAIN_MODE).requireNonNull()
        trainId = requireArguments().getString(ATTR_TRAIN_ID).requireNonNull()
        prepareClickListeners()
        prepareContentList()
        observeData()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun prepareClickListeners() {
        toolbar_back.setOnClickListener {
            findNavController().popBackStack()
        }
        fab_delete.setOnClickListener {
            trainShowVM.delete(trainId)
            findNavController().popBackStack()
        }
    }

    private fun prepareContentList() {
        adapter = TrainResultAdapter()
        view_list.layoutManager = LinearLayoutManager(requireContext())
        view_list.adapter = adapter
    }

    private fun observeData() {
        when (trainMode) {
            TrainOverviewFragment.METHOD_SHOW -> trainShowVM.observeTrainShowResult(trainId)
                .observe(viewLifecycleOwner, Observer {
                    adapter.setList(it)
                })
            TrainOverviewFragment.METHOD_WRITE -> trainWriteVM.observeTrainWriteResult(trainId)
                .observe(viewLifecycleOwner, Observer {
                    adapter.setList(it)
                })
            else -> throw Exception()
        }
    }

    companion object {
        const val ATTR_TRAIN_MODE = "train_mode"
        const val ATTR_TRAIN_ID = "train_id"
    }
}