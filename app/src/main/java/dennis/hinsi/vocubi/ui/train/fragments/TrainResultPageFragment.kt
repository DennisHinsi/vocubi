/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.train.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseFragment
import dennis.hinsi.vocubi.ui.adapter.TrainOverviewAdapter
import dennis.hinsi.vocubi.ui.train.fragments.TrainOverviewFragment.Companion.METHOD_SHOW
import dennis.hinsi.vocubi.ui.train.fragments.TrainOverviewFragment.Companion.METHOD_WRITE
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainOverViewVM
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.result_pager_item_layout.*
import javax.inject.Inject

class TrainResultPageFragment : BaseFragment() {

    @Inject
    lateinit var trainOverViewVM: TrainOverViewVM

    private var trainMethod: Int = -1
    private lateinit var adapter: TrainOverviewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.result_pager_item_layout, container, false
    )

    override fun prepareUI() {
        adapter = TrainOverviewAdapter()
        view_list.layoutManager = LinearLayoutManager(requireContext())
        view_list.adapter = adapter
    }

    override fun prepareOnClickListeners() {
        adapter.clickStream
            .doOnError(::rxStreamError)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it.first) {
                    METHOD_SHOW -> findNavController().navigate(
                        R.id.trainShowResultFragment,
                        bundleOf(
                            TrainResultFragment.ATTR_TRAIN_ID to it.second,
                            TrainResultFragment.ATTR_TRAIN_MODE to METHOD_SHOW
                        )
                    )

                    METHOD_WRITE -> findNavController().navigate(
                        R.id.trainShowResultFragment,
                        bundleOf(
                            TrainResultFragment.ATTR_TRAIN_ID to it.second,
                            TrainResultFragment.ATTR_TRAIN_MODE to METHOD_WRITE
                        )
                    )
                }
            }
            .addToSubscriptions()
    }

    override fun observeData() {
        when (trainMethod) {
            METHOD_SHOW -> trainOverViewVM.observeTrainShow()
            METHOD_WRITE -> trainOverViewVM.observeTrainWrite()
            else -> null
        }?.observe(viewLifecycleOwner, Observer { adapter.setList(it) })
    }

    companion object {
        fun newTrainShowInstance() = TrainResultPageFragment().apply {
            this.trainMethod = METHOD_SHOW
        }

        fun newTrainWriteInstance() = TrainResultPageFragment().apply {
            this.trainMethod = METHOD_WRITE
        }
    }
}