/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.train.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dennis.hinsi.domain.model.TrainShow
import dennis.hinsi.domain.model.TrainShowResult
import dennis.hinsi.domain.model.Voc
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseFragment
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainShowVM
import kotlinx.android.synthetic.main.frg_train_show.*
import java.util.*
import javax.inject.Inject

class TrainShowFragment : BaseFragment() {

    @Inject
    lateinit var trainShowVM: TrainShowVM

    private val args: TrainShowFragmentArgs by navArgs()

    private var trainShow: TrainShow? = null
    private var list: List<Voc>? = null
    private var currentVoc: Voc? = null
    private var currentIndex: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.frg_train_show, container, false
    )

    override fun prepareUI() = Unit

    override fun prepareOnClickListeners() {
        fab_train_rotate.setOnClickListener {
            currentVoc?.expressionLang1?.let {
                train_card_text.text = it
            }
        }
        fab_train_correct.setOnClickListener {
            currentVoc?.let {
                pushResult(it, true)
            }
            loadNext()
        }
        fab_train_wrong.setOnClickListener {
            currentVoc?.let {
                pushResult(it, false)
            }
            loadNext()
        }
    }

    override fun observeData() {
        trainShowVM.loadVocList(args.BOOKID).observe(viewLifecycleOwner, Observer { vocList ->
            if (vocList.isNotEmpty()) {
                train_show_progress.max = vocList.size
                train_show_progress.progress = 0
                training(vocList)
            } else {
                Toast.makeText(requireContext(), "NO VOCABULARIES FOUND", Toast.LENGTH_SHORT).show()
                findNavController().popBackStack()
            }
        })

        trainShowVM.loadBook(args.BOOKID).observe(viewLifecycleOwner, Observer {
            toolbar.text = it.title
        })
    }

    private fun pushResult(voc: Voc, isCorrect: Boolean) {
        if (trainShow == null)
            prepareDataObjects()

        trainShowVM.insert(
            TrainShowResult(
                id = UUID.randomUUID().toString(),
                trainId = trainShow!!.id,
                vocId = voc.id,
                bookId = args.BOOKID,
                time = System.currentTimeMillis(),
                isResultCorrect = isCorrect
            )
        )
    }

    private fun loadNext() {
        list?.let { vocList ->
            if (currentIndex + 1 < vocList.size) {
                currentIndex++
                train_show_progress.progress = currentIndex
                currentVoc = vocList[currentIndex]
                currentVoc?.expressionLang0?.let { str ->
                    train_card_text.text = str
                }
            } else {
                trainShow!!.IsDone = true
                findNavController().navigate(
                    R.id.action_trainShowFragment_to_trainShowResultFragment,
                    bundleOf(TrainResultFragment.ATTR_TRAIN_ID to trainShow!!.id)
                )
            }
        }
    }

    private fun prepareDataObjects() {
        trainShow = TrainShow(
            id = UUID.randomUUID().toString(),
            bookId = args.BOOKID,
            time = System.currentTimeMillis(),
            IsDone = false
        ).also { trainShow ->
            trainShowVM.insert(trainShow)
        }
    }

    private fun training(list: List<Voc>) {
        this.list = list
        currentVoc = list[0]
        currentVoc?.expressionLang0?.let { str ->
            train_card_text.text = str
        }
    }
}