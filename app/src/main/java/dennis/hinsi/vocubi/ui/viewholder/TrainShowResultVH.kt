/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.viewholder

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.domain.model.TrainShowResultExtended
import dennis.hinsi.vocubi.R

class TrainShowResultVH(private val view: View) : RecyclerView.ViewHolder(view) {

    private val textView = view.findViewById(R.id.item_text) as TextView
    private val divider = view.findViewById(R.id.item_divider) as View

    fun bind(item: TrainShowResultExtended, isLast: Boolean) {
        @SuppressLint("SetTextI18n")
        textView.text = item.voc.expressionLang0 + " -> " + item.voc.expressionLang1

        val drawable = view.resources.getDrawable(
            if (item.trainShowResult.isResultCorrect)
                R.drawable.ic_thumbs_up_24
            else
                R.drawable.ic_thumbs_down_24,
            view.context.theme
        )

        textView.compoundDrawableTintList = ColorStateList.valueOf(
            view.resources.getColor(
                if (item.trainShowResult.isResultCorrect)
                    R.color.lang_accent_green
                else
                    R.color.lang_accent_red,
                view.context.theme
            )
        )

        textView.setCompoundDrawablesWithIntrinsicBounds(
            null, null, drawable, null
        )
        divider.visibility = if (isLast) View.GONE else View.VISIBLE
    }

    companion object {
        fun newInstance(parent: ViewGroup) = TrainShowResultVH(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_train_show_result, parent, false)
        )
    }
}