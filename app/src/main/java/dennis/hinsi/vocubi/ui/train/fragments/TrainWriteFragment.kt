/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.train.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dennis.hinsi.domain.model.TrainWrite
import dennis.hinsi.domain.model.TrainWriteResult
import dennis.hinsi.domain.model.Voc
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseFragment
import dennis.hinsi.vocubi.ui.train.viewmodels.TrainWriteVM
import kotlinx.android.synthetic.main.frg_train_write.*
import java.util.*
import javax.inject.Inject

class TrainWriteFragment : BaseFragment() {

    @Inject
    lateinit var trainWriteVM: TrainWriteVM

    private val args: TrainWriteFragmentArgs by navArgs()

    private var trainId: String? = null
    private var list: List<Voc>? = null
    private var index: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.frg_train_write, container, false
    )

    override fun prepareUI() = Unit

    override fun prepareOnClickListeners() {
        reset.setOnClickListener {
            write_picker_view.reset()
        }
    }

    override fun observeData() {
        trainWriteVM.loadVocList(args.BOOKID).observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                train_write_progress.max = it.size
                train_write_progress.progress = 0
                training(it)
            } else {
                Toast.makeText(requireContext(), "NO VOCABULARIES FOUND", Toast.LENGTH_SHORT).show()
                findNavController().popBackStack()
            }
        })

        trainWriteVM.loadBook(args.BOOKID).observe(viewLifecycleOwner, Observer {
            toolbar.text = it.title
        })
    }

    private fun training(list: List<Voc>) {
        this.list = list
        loadNext()
    }

    private fun loadNext() {
        if (index < list?.size ?: 0) {
            if (trainId == null)
                initTrainingSession()

            list?.let { vocList ->
                val tempIndex = index
                train_write_question.text = vocList[tempIndex].expressionLang0
                write_picker_view.setLetters(vocList[tempIndex].expressionLang1, 3)
                write_picker_view.onComplete { isResultCorrect, result ->
                    postResult(vocList[tempIndex], isResultCorrect, result)

                    loadNext()
                }
            }
            train_write_progress.progress ++
            index++
        } else {
            findNavController().navigate(
                R.id.action_trainWriteFragment_to_trainShowResultFragment,
                bundleOf(
                    TrainResultFragment.ATTR_TRAIN_ID to trainId,
                    TrainResultFragment.ATTR_TRAIN_MODE to TrainOverviewFragment.METHOD_WRITE
                )
            )
        }
    }

    private fun initTrainingSession() {
        trainId = UUID.randomUUID().toString()
        trainWriteVM.insert(
            TrainWrite(
                id = trainId!!,
                bookId = args.BOOKID,
                created = System.currentTimeMillis()
            )
        )
    }

    private fun postResult(voc: Voc, resultCorrect: Boolean, result: String) {
        trainWriteVM.insert(
            TrainWriteResult(
                id = UUID.randomUUID().toString(),
                bookId = args.BOOKID,
                vocId = voc.id,
                trainId = trainId!!,
                isResultCorrect = resultCorrect,
                result = result
            )
        )
    }
}

