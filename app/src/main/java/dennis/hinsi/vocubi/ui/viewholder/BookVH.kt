/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.viewholder

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import dennis.hinsi.domain.model.Book
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseVH
import dennis.hinsi.vocubi.util.getTimestampString
import io.reactivex.rxjava3.subjects.PublishSubject

class BookVH(private val view: View) : BaseVH<Book, String>(view) {

    private val textView = view.findViewById(R.id.item_text) as TextView
    private val chipLang0 = view.findViewById(R.id.item_lang_0_name) as TextView
    private val chipLang1 = view.findViewById(R.id.item_lang_1_name) as TextView
    private val timeStamp = view.findViewById(R.id.item_timestamp) as TextView
    private val divider = view.findViewById(R.id.overview_item_divider) as View

    override fun bind(item: Book, isLast: Boolean, clickStream: PublishSubject<String>) {
        view.setOnClickListener {
            clickStream.onNext(item.id)
        }
        textView.text = item.title
        chipLang0.text = item.language_name_0
        chipLang1.text = item.language_name_1
        timeStamp.text = item.modified.getTimestampString()
        chipLang0.setTextColor(ColorStateList.valueOf(item.accent_0))
        chipLang1.setTextColor(ColorStateList.valueOf(item.accent_1))
        divider.visibility = if (isLast) View.GONE else View.VISIBLE
    }

    companion object {
        fun newInstance(parent: ViewGroup) = BookVH(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_book, parent, false)
        )
    }
}
