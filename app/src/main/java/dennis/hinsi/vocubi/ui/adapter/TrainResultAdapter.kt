/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.domain.model.TrainShowResultExtended
import dennis.hinsi.domain.model.TrainWriteResultExtended
import dennis.hinsi.vocubi.ui.exceptions.UnknownViewHolderException
import dennis.hinsi.vocubi.ui.viewholder.TrainShowResultVH
import dennis.hinsi.vocubi.ui.viewholder.TrainWriteResultVH
import dennis.hinsi.vocubi.ui.train.fragments.TrainOverviewFragment

class TrainResultAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list = emptyList<Any>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            TrainOverviewFragment.METHOD_SHOW -> TrainShowResultVH.newInstance(parent)
            TrainOverviewFragment.METHOD_WRITE -> TrainWriteResultVH.newInstance(parent)
            else -> throw UnknownViewHolderException()
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TrainShowResultVH -> holder.bind(
                list[position] as TrainShowResultExtended,
                position + 1 == list.size
            )
            is TrainWriteResultVH -> holder.bind(
                list[position] as TrainWriteResultExtended,
                position + 1 == list.size
            )
        }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int = when (list[position]) {
        is TrainShowResultExtended -> TrainOverviewFragment.METHOD_SHOW
        is TrainWriteResultExtended -> TrainOverviewFragment.METHOD_WRITE
        else -> throw UnknownViewHolderException()
    }

    fun setList(list: List<Any>) {
        this.list = list
        notifyDataSetChanged()
    }
}

