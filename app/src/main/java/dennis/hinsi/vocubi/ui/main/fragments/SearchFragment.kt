/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.widget.afterTextChangeEvents
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseFragment
import dennis.hinsi.vocubi.ui.adapter.SearchAdapter
import dennis.hinsi.vocubi.ui.main.viewmodels.SearchVM
import dennis.hinsi.vocubi.util.AttrUtil
import dennis.hinsi.vocubi.util.hideSoftKeyboard
import dennis.hinsi.vocubi.util.showSoftKeyboard
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.frg_main_search.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchFragment : BaseFragment() {

    @Inject
    lateinit var searchVM: SearchVM

    private lateinit var adapter: SearchAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_main_search, container, false)

    override fun onPause() {
        search_input.hideSoftKeyboard(requireActivity())
        super.onPause()
    }

    override fun prepareUI() {
        adapter = SearchAdapter()
        view_list.layoutManager = LinearLayoutManager(requireContext())
        view_list.adapter = adapter
        search_input.showSoftKeyboard(requireActivity())
    }

    override fun prepareOnClickListeners() {
        search_back.clicks()
            .debounce(AttrUtil.VIEW_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .doOnError(::rxStreamError)
            .subscribe { findNavController().popBackStack() }
            .addToSubscriptions()

        adapter.bookClickStream
            .distinctUntilChanged()
            .doOnError(::rxStreamError)
            .subscribe { book ->
                findNavController().navigate(
                    SearchFragmentDirections.actionSearchFragmentToBookFragment(book.id)
                )
            }
            .addToSubscriptions()

        adapter.vocabularyClickStream
            .distinctUntilChanged()
            .doOnError(::rxStreamError)
            .subscribe { voc ->
                findNavController().navigate(
                    SearchFragmentDirections.actionSearchFragmentToBookFragment(voc.id)
                )
            }
            .addToSubscriptions()
    }

    override fun observeData() {
        search_input.afterTextChangeEvents()
            .debounce(AttrUtil.TEXT_INPUT_TIME_OUT, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { updateData(it.editable?.toString()) }
            .addToSubscriptions()
    }

    private fun updateData(query: String?) {
        adapter.clear()
        if (query != null && query.isNotEmpty()) {
            searchVM.search(query).observe(viewLifecycleOwner, Observer {
                adapter.update(query, it)
            })
        }
    }
}