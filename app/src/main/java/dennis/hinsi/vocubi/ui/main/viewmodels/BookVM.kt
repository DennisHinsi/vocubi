/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.main.viewmodels

import dennis.hinsi.domain.model.Book
import dennis.hinsi.domain.usecase.BookEditorUC
import dennis.hinsi.domain.usecase.BookProviderUC
import dennis.hinsi.domain.usecase.VocProviderUC
import dennis.hinsi.vocubi.base.CoroutineViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class BookVM @Inject constructor(
    private val bookProviderUC: BookProviderUC,
    private val bookEditorUC: BookEditorUC,
    private val vocProviderUC: VocProviderUC
) : CoroutineViewModel() {

    fun load(bookId: String) = bookProviderUC.load(bookId)

    fun observeVocList(bookId: String) = vocProviderUC.observe(bookId)

    fun update(
        bookId: String,
        title: String,
        lang0: String,
        lang1: String,
        lang0Accent: Int,
        lang1Accent: Int
    ) = backgroundScope.launch(coroutineExceptions) {
        bookEditorUC.update(bookId, title, lang0, lang1, lang0Accent, lang1Accent)
    }

    fun create(book: Book) = backgroundScope.launch(coroutineExceptions) {
        bookEditorUC.create(book)
    }

    fun delete(bookId: String) = backgroundScope.launch(coroutineExceptions) {
        bookEditorUC.delete(bookId)
    }
}