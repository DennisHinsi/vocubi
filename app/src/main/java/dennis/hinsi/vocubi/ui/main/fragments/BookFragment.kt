/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding4.view.clicks
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseFragment
import dennis.hinsi.vocubi.ui.adapter.VocAdapter
import dennis.hinsi.vocubi.ui.main.viewmodels.BookVM
import dennis.hinsi.vocubi.util.AttrUtil
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.frg_main_book.*
import kotlinx.android.synthetic.main.frg_main_book.view_list
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BookFragment : BaseFragment() {

    @Inject
    lateinit var bookVM: BookVM

    private lateinit var adapter: VocAdapter
    private val args: BookFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_main_book, container, false)

    override fun prepareUI() {
        adapter = VocAdapter()
        view_list.layoutManager = LinearLayoutManager(requireContext())
        view_list.adapter = adapter
    }

    override fun prepareOnClickListeners() {
        book_back.clicks()
            .debounce(AttrUtil.VIEW_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { findNavController().popBackStack() }
            .addToSubscriptions()

        bottom_bar_add_voc.clicks()
            .debounce(AttrUtil.VIEW_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                findNavController().navigate(
                    BookFragmentDirections.actionBookFragmentToVocEditorFragment(args.bookId)
                )
            }
            .addToSubscriptions()

        bottom_bar_edit.clicks()
            .debounce(AttrUtil.VIEW_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                findNavController().navigate(
                    BookFragmentDirections.actionBookFragmentToEditBookFragment(args.bookId)
                )
            }
            .addToSubscriptions()

        adapter.clickStream
            .debounce(AttrUtil.ADAPTER_CLICK_TIME_OUT, TimeUnit.MILLISECONDS)
            .doOnError(::rxStreamError)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { vocId ->
                findNavController().navigate(
                    BookFragmentDirections.actionBookFragmentToEditVocFragment(vocId)
                )
            }
            .addToSubscriptions()
    }

    override fun observeData() {
        bookVM.load(args.bookId).observe(viewLifecycleOwner, Observer { book ->
            if (book == null)
                // SPECIAL CASE: WHEN USER DELETES A BOOK USED IN THIS FRAGMENT THE VIEWMODEL
                // RETURNS A NULL OBJECT.
                // THE MAIN PROBLEM IS THAT WE DON'T KNOW IF THE BOOK WILL BE DELETED IN
                // EditBookFragment. SO WE CANNOT CHANGE THE BACKSTACK WHEN WE CALL IT.
                //
                // PERFECT BEHAVIOUR:
                // BookFragment -> EditBookFragment (DELETE BOOK) -> OverviewFragment
                // BookFragment -> EditBookFragment (UPDATE BOOK) -> BookFragment
                //
                // WORKAROUND:
                // BookFragment -> EditBookFragment (DELETE BOOK) -> BookFragment(BOOK IS NULL) -> OverviewFragment
                findNavController().popBackStack()
            else {
                book_toolbar.text = book.title
                lang_left_header.setBackgroundColor(book.accent_0)
                lang_right_header.setBackgroundColor(book.accent_1)
                lang_left_header.text = book.language_name_0
                lang_right_header.text = book.language_name_1
            }
        })

        bookVM.observeVocList(args.bookId).observe(viewLifecycleOwner, Observer { vocList ->
            adapter.list = vocList
        })
    }
}
