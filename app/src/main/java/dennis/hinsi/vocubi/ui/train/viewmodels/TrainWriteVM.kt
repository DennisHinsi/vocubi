/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.train.viewmodels

import androidx.lifecycle.liveData
import dennis.hinsi.domain.model.TrainWrite
import dennis.hinsi.domain.model.TrainWriteResult
import dennis.hinsi.domain.usecase.*
import dennis.hinsi.vocubi.base.CoroutineViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class TrainWriteVM @Inject constructor(
    private val bookProviderUC: BookProviderUC,
    private val vocProviderUC: VocProviderUC,
    private val trainWriteEditorUC: TrainWriteEditorUC,
    private val trainWriteResultEditorUC: TrainWriteResultEditorUC,
    private val trainWriteResultProviderUC: TrainWriteResultProviderUC
) : CoroutineViewModel() {
    fun loadBook(bookId: String) = liveData(Dispatchers.IO) {
        emitSource(bookProviderUC.load(bookId))
    }

    fun loadVocList(bookId: String) = liveData(Dispatchers.IO) {
        emit(vocProviderUC.loadAllSingle(bookId))
    }

    fun insert(trainWrite: TrainWrite) = backgroundScope.launch(coroutineExceptions) {
        trainWriteEditorUC.insert(trainWrite)
    }

    fun insert(trainWriteResult: TrainWriteResult) = backgroundScope.launch(coroutineExceptions) {
        trainWriteResultEditorUC.insert(trainWriteResult)
    }

    fun observeTrainWriteResult(trainId: String) = trainWriteResultProviderUC.observe(trainId)
}