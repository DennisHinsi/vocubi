/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.main.viewmodels

import androidx.lifecycle.liveData
import dennis.hinsi.domain.model.Voc
import dennis.hinsi.domain.usecase.VocEditorUC
import dennis.hinsi.domain.usecase.VocProviderUC
import dennis.hinsi.vocubi.base.CoroutineViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

class VocVM @Inject constructor(
    private val vocEditorUC: VocEditorUC,
    private val vocProviderUC: VocProviderUC
) : CoroutineViewModel() {

    fun load(vocId: String) = liveData {
        emit(vocProviderUC.load(vocId))
    }

    fun delete(vocId: String) = backgroundScope.launch(coroutineExceptions) {
        vocEditorUC.delete(vocId)
    }

    fun create(voc: Voc) = backgroundScope.launch(coroutineExceptions) {
        vocEditorUC.create(voc)
    }

    fun update(
        vocId: String,
        exp0: String,
        exp1: String
    ) = backgroundScope.launch(coroutineExceptions) {
        vocEditorUC.update(vocId, exp0, exp1)
    }
}