/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.widget.textChangeEvents
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.base.BaseSheetDialogFragment
import dennis.hinsi.vocubi.ui.main.viewmodels.VocVM
import dennis.hinsi.vocubi.util.AttrUtil
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.dialog_edit_vocabulary.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class EditVocFragment : BaseSheetDialogFragment() {

    @Inject
    lateinit var vocVM: VocVM

    private var isInput0Empty: Boolean = true
    private var isInput1Empty: Boolean = true
    private val args: EditVocFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_edit_vocabulary, container, false)

    override fun prepareUI() = Unit

    override fun prepareOnClickListeners() {
        edit_voc_cancel.clicks()
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { dismiss() }
            .addToSubscriptions()

        edit_voc_submit.clicks()
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (!isInput0Empty && !isInput1Empty) {
                    vocVM.update(
                        vocId = args.vocId,
                        exp0 = edit_voc_0.text.toString(),
                        exp1 = edit_voc_1.text.toString()
                    )
                    dismiss()
                }
            }
            .addToSubscriptions()

        edit_voc_delete.clicks()
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                vocVM.delete(args.vocId)
                dismiss()
            }
            .addToSubscriptions()

        edit_voc_0.textChangeEvents()
            .debounce(AttrUtil.TEXT_INPUT_TIME_OUT, TimeUnit.MILLISECONDS)
            .subscribe {
                isInput0Empty = it.toString().isEmpty()
                updateSubmitButton()
            }
            .addToSubscriptions()

        edit_voc_1.textChangeEvents()
            .debounce(AttrUtil.TEXT_INPUT_TIME_OUT, TimeUnit.MILLISECONDS)
            .subscribe {
                isInput1Empty = it.toString().isEmpty()
                updateSubmitButton()
            }
            .addToSubscriptions()
    }

    override fun observeData() {
        vocVM.load(args.vocId).observe(viewLifecycleOwner, Observer {
            edit_voc_0.setText(it.expressionLang0)
            edit_voc_1.setText(it.expressionLang1)
        })
    }

    private fun updateSubmitButton() {
        edit_voc_submit.setColorFilter(
            ResourcesCompat.getColor(
                resources,
                getSubmitColor(),
                requireContext().theme
            )
        )
    }

    private fun getSubmitColor(): Int =
        if (!isInput0Empty && !isInput1Empty)
            R.color.black
        else
            R.color.disabled_gray
}