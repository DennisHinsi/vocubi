/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.domain.model.TrainShow
import dennis.hinsi.vocubi.R
import io.reactivex.rxjava3.subjects.PublishSubject
import dennis.hinsi.vocubi.ui.train.fragments.TrainOverviewFragment.Companion.METHOD_SHOW
import java.text.SimpleDateFormat
import java.util.*

class TrainOverviewShowVH(private val view: View) : RecyclerView.ViewHolder(view) {

    private val title = view.findViewById(R.id.item_text) as TextView
    private val progress = view.findViewById(R.id.item_progress) as SeekBar
    private val progressText = view.findViewById(R.id.item_progress_text) as TextView
    private val progressResultText = view.findViewById(R.id.item_progress_result_text) as TextView
    private val time = view.findViewById(R.id.item_time) as TextView
    private val divider = view.findViewById(R.id.item_divider) as View


    fun bind(item: TrainShow, isLast: Boolean, clickStream: PublishSubject<Pair<Int, String>>) {
        view.setOnClickListener {
            clickStream.onNext(METHOD_SHOW to item.id)
        }
        title.text = view.resources.getString(R.string.train_overview_item_title, item.title)
        val maxCountTrained = item.correctCount + item.wrongCount
        progress.max = maxCountTrained
        progress.min = 0
        progress.progress = item.correctCount
        progress.isEnabled = false
        progress.isHapticFeedbackEnabled = false

        progressText.text = view.resources.getString(
            R.string.train_overview_item_progress_text,
            maxCountTrained,
            item.maxCount
        )

        progressResultText.text = view.resources.getString(
            R.string.train_overview_item_progress_result_text,
            item.correctCount,
            maxCountTrained
        )

        time.text = view.resources.getString(
            R.string.train_overview_item_result_text,
            SimpleDateFormat("dd.MM.yyyy hh:mm", Locale.ROOT).format(Date(item.time))
        )
        divider.visibility = if (isLast) View.INVISIBLE else View.VISIBLE
    }

    companion object {
        fun newInstance(parent: ViewGroup) =
            TrainOverviewShowVH(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_train, parent, false)
            )
    }
}
