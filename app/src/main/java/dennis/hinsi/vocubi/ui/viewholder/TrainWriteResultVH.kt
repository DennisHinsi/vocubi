/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.viewholder

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.domain.model.TrainWriteResultExtended
import dennis.hinsi.vocubi.R

class TrainWriteResultVH(private val view: View) : RecyclerView.ViewHolder(view) {

    private val textViewExpected = view.findViewById(R.id.item_text_expected) as TextView
    private val textViewResult = view.findViewById(R.id.item_text_result) as TextView
    private val icon = view.findViewById(R.id.item_icon) as ImageView
    private val divider = view.findViewById(R.id.item_divider) as View

    fun bind(item: TrainWriteResultExtended, isLast: Boolean) {
        textViewExpected.text = item.voc.expressionLang1
        textViewResult.text = item.trainWriteResult.result

        setDrawable(item)
        setDrawableTint(item)

        divider.visibility = if (isLast) View.GONE else View.VISIBLE
    }

    private fun setDrawable(item: TrainWriteResultExtended) {
        val drawable = view.resources.getDrawable(
            if (item.trainWriteResult.isResultCorrect)
                R.drawable.ic_thumbs_up_24
            else
                R.drawable.ic_thumbs_down_24,
            view.context.theme
        )
        icon.setImageDrawable(drawable)
    }

    private fun setDrawableTint(item: TrainWriteResultExtended) {
        icon.imageTintList = ColorStateList.valueOf(
            view.resources.getColor(
                if (item.trainWriteResult.isResultCorrect)
                    R.color.lang_accent_green
                else
                    R.color.lang_accent_red,
                view.context.theme
            )
        )
    }

    companion object {
        fun newInstance(parent: ViewGroup) = TrainWriteResultVH(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_train_write_result, parent, false)
        )
    }
}