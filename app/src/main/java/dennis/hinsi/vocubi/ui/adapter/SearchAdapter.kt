/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.domain.model.Book
import dennis.hinsi.domain.model.Voc
import dennis.hinsi.vocubi.ui.viewholder.SearchBookVH
import dennis.hinsi.vocubi.ui.viewholder.SearchVocVH
import dennis.hinsi.vocubi.ui.exceptions.UnknownViewHolderException
import io.reactivex.rxjava3.subjects.PublishSubject


class SearchAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list = mutableListOf<Any>()
    private var query: String = ""

    val bookClickStream: PublishSubject<Book> = PublishSubject.create<Book>()
    val vocabularyClickStream: PublishSubject<Voc> = PublishSubject.create<Voc>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder = when (viewType) {
        HOLDER_BOOK -> SearchBookVH.newInstance(parent)
        HOLDER_VOC -> SearchVocVH.newInstance(parent)
        else -> throw UnknownViewHolderException()
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) = when (holder) {
        is SearchBookVH -> holder.bind(
            item = list[position] as Book,
            query = query,
            isLast = position + 1 == list.size,
            stream = bookClickStream
        )

        is SearchVocVH -> holder.bind(
            item = list[position] as Voc,
            query = query,
            isLast = position + 1 == list.size,
            stream = vocabularyClickStream
        )

        else -> throw UnknownViewHolderException()
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int = when (list[position]) {
        is Book -> HOLDER_BOOK
        is Voc -> HOLDER_VOC
        else -> throw UnknownViewHolderException()
    }

    fun update(query: String, items: List<Any>) {
        this.query = query
        list.addAll(items)
        notifyDataSetChanged()
    }

    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    private companion object {
        const val HOLDER_BOOK = 0
        const val HOLDER_VOC = 1
    }
}

