/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.settings.fragments

import android.content.Intent
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import dennis.hinsi.vocubi.BuildConfig
import dennis.hinsi.vocubi.R


class SettingsOverviewFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_settings_overview, rootKey)
        findPreference<Preference>("pref_build")?.summary = BuildConfig.BUILD_TYPE
        findPreference<Preference>("pref_version")?.summary = BuildConfig.VERSION_NAME
        findPreference<Preference>("pref_licenses")?.setOnPreferenceClickListener {
            OssLicensesMenuActivity.setActivityTitle(getString(R.string.settings_licences))
            startActivity(Intent(requireContext(), OssLicensesMenuActivity::class.java))
            true
        }
    }

}