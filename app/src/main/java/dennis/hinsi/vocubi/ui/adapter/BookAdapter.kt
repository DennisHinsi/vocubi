/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.adapter

import android.view.ViewGroup
import dennis.hinsi.domain.model.Book
import dennis.hinsi.vocubi.base.BaseAdapter
import dennis.hinsi.vocubi.ui.viewholder.BookVH

class BookAdapter : BaseAdapter<Book, String, BookVH>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BookVH = BookVH.newInstance(parent)
}