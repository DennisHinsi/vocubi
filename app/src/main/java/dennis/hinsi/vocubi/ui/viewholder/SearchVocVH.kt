/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.viewholder

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.domain.model.Voc
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.util.setText
import io.reactivex.rxjava3.subjects.PublishSubject

class SearchVocVH(private val view: View) : RecyclerView.ViewHolder(view) {

    private var itemLang0 = view.findViewById(R.id.item_lang_0) as TextView
    private var itemLang1 = view.findViewById(R.id.item_lang_1) as TextView
    private val divider = view.findViewById(R.id.item_divider) as View

    fun bind(item: Voc, query: String, isLast: Boolean, stream: PublishSubject<Voc>) {
        view.setOnClickListener {
            stream.onNext(item)
        }
        itemLang0.setText(item.expressionLang0, query, Color.BLUE)
        itemLang1.setText(item.expressionLang1, query, Color.BLUE)
        divider.visibility = if (isLast) View.INVISIBLE else View.VISIBLE
    }

    companion object {
        fun newInstance(parent: ViewGroup) = SearchVocVH(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_search_result_vocabulary, parent, false)
        )
    }
}