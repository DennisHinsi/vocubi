/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.adapter

import android.content.res.Resources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.ui.train.fragments.TrainOverviewFragment
import dennis.hinsi.vocubi.ui.train.fragments.TrainResultPageFragment

class ResultPagerAdapter(
    private val resources: Resources,
    fragmentManager: FragmentManager
) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int = 2

    override fun getItem(position: Int): Fragment = when (position) {
        TrainOverviewFragment.METHOD_SHOW -> TrainResultPageFragment.newTrainShowInstance()
        TrainOverviewFragment.METHOD_WRITE -> TrainResultPageFragment.newTrainWriteInstance()
        else -> throw Exception()
    }

    override fun getPageTitle(position: Int): CharSequence? = when (position) {
        TrainOverviewFragment.METHOD_SHOW -> resources.getString(R.string.common_train_mode_show)
        TrainOverviewFragment.METHOD_WRITE -> resources.getString(R.string.common_train_mode_write)
        else -> null
    }
}