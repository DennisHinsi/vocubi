/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.ui.viewholder

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import dennis.hinsi.domain.model.Book
import dennis.hinsi.vocubi.R
import dennis.hinsi.vocubi.util.setText
import io.reactivex.rxjava3.subjects.PublishSubject

class SearchBookVH(private val view: View) : RecyclerView.ViewHolder(view) {

    private val title = view.findViewById(R.id.item_text) as TextView
    private val chipLang0 = view.findViewById(R.id.item_lang_0_name) as Chip
    private val chipLang1 = view.findViewById(R.id.item_lang_1_name) as Chip
    private val divider = view.findViewById(R.id.item_divider) as View

    fun bind(item: Book, query: String, isLast: Boolean, stream: PublishSubject<Book>) {
        view.setOnClickListener {
            stream.onNext(item)
        }
        title.setText(item.title, query, Color.BLUE)

        chipLang0.text = item.language_name_0
        chipLang1.text = item.language_name_1
        divider.visibility = if (isLast) View.INVISIBLE else View.VISIBLE
    }

    companion object {
        fun newInstance(parent: ViewGroup) = SearchBookVH(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_search_result_book, parent, false)
        )
    }
}