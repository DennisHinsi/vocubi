/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.util

import android.app.Activity
import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import dennis.hinsi.vocubi.R
import java.text.SimpleDateFormat
import java.util.*

fun Context.loadAccentColors(): List<Int> = listOf(
    resources.getColor(R.color.lang_accent_yellow, theme),
    resources.getColor(R.color.lang_accent_yellow1, theme),
    resources.getColor(R.color.lang_accent_blue, theme),
    resources.getColor(R.color.lang_accent_blue1, theme),
    resources.getColor(R.color.lang_accent_green, theme),
    resources.getColor(R.color.lang_accent_green1, theme),
    resources.getColor(R.color.lang_accent_red, theme),
    resources.getColor(R.color.lang_accent_red1, theme),
    resources.getColor(R.color.lang_accent_purple, theme),
    resources.getColor(R.color.lang_accent_purple1, theme),
    resources.getColor(R.color.lang_accent_gray, theme),
    resources.getColor(R.color.lang_accent_gray1, theme)
)

fun Long.getTimestampString(): String = SimpleDateFormat(
    "dd.MM.yyyy hh:mm",
    Locale.getDefault(Locale.Category.FORMAT)
).format(Date(this))

fun <Any> Any?.requireNonNull(): Any {
    if (this == null)
        throw KotlinNullPointerException()
    else
        return this
}

fun <T> Collection<T>.forEachApply(block: (T) -> Unit): Collection<T> {
    forEach {
        block(it)
    }
    return this
}

fun EditText.showSoftKeyboard(activity: Activity) {
    requestFocus()
    (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .showSoftInput(this, 0)
}

fun EditText.hideSoftKeyboard(activity: Activity) {
    (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(this.windowToken, 0)
}

fun TextView.setText(text: String, query: String, color: Int) {
    this.text = SpannableString(text).apply {
        text.findIndicesOfQuery(query).forEach {
            setSpan(
                ForegroundColorSpan(color),
                it.first,
                it.second,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}

fun String.findIndicesOfQuery(query: String): List<Pair<Int, Int>> =
    resolveIndices(
        toLowerCase(Locale.ROOT),
        query.toLowerCase(Locale.ROOT).trimEnd(),
        0,
        mutableListOf()
    )

private fun resolveIndices(
    source: String,
    query: String,
    index: Int,
    result: MutableList<Pair<Int, Int>>
): MutableList<Pair<Int, Int>> {
    val indexMax = index + query.length
    if (indexMax <= source.length) {
        if (source.substring(index, indexMax) == query)
            result.add(index to indexMax)
        resolveIndices(source, query, index + 1, result)
    }
    return result
}