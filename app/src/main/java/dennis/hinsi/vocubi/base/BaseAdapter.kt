/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.vocubi.base

import androidx.recyclerview.widget.RecyclerView
import io.reactivex.rxjava3.subjects.PublishSubject

abstract class BaseAdapter<LIST_TYPE, CALLBACK_TYPE, VH : BaseVH<LIST_TYPE, CALLBACK_TYPE>> :
    RecyclerView.Adapter<VH>() {

    val clickStream = PublishSubject.create<CALLBACK_TYPE>()

    var list = emptyList<LIST_TYPE>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position], position + 1 == list.size, clickStream)
    }

    override fun getItemCount(): Int = list.size

}