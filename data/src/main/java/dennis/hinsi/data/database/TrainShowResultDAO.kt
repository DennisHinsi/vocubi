/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import dennis.hinsi.domain.model.TrainShowResult
import dennis.hinsi.domain.model.TrainShowResultExtended

@Dao
interface TrainShowResultDAO {
    @Insert
    suspend fun insert(trainShowResult: TrainShowResult)

    @Update
    suspend fun update(trainShowResult: TrainShowResult)

    @Transaction
    @Query("SELECT * FROM train_show_result WHERE train_id = :trainId")
    fun observe(trainId: String): LiveData<List<TrainShowResultExtended>>

    @Query("SELECT * FROM train_show_result WHERE id = :id")
    suspend fun load(id: String): TrainShowResult

    @Query("SELECT * FROM train_show_result WHERE train_id = :trainId")
    suspend fun loadAll(trainId: String): List<TrainShowResult>

    @Query("DELETE FROM train_show_result WHERE train_id = :id")
    suspend fun delete(id: String)

    @Query("DELETE FROM train_show_result WHERE book_id = :bookId")
    suspend fun deleteAllOfBook(bookId: String)
}