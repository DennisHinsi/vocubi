/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import dennis.hinsi.domain.model.Book
import dennis.hinsi.domain.model.BookExtended

@Dao
interface BookDAO {
    @Insert
    suspend fun insert(book: Book)

    @Query("SELECT * FROM BookContent")
    fun observe(): LiveData<List<Book>>

    @Transaction
    @Query("SELECT * FROM BookContent")
    fun observeExtended(): LiveData<List<BookExtended>>

    @Query("SELECT * FROM BookContent WHERE title LIKE  '%' || :query || '%'")
    fun search(query: String): LiveData<List<Book>>

    @Query("SELECT * FROM BookContent WHERE id = :id")
    fun load(id: String): LiveData<Book>

    @Query("SELECT * FROM BookContent WHERE id = :id")
    suspend fun loadSingle(id: String): Book

    @Update
    suspend fun update(book: Book)

    @Query("DELETE FROM BookContent WHERE id = :id")
    suspend fun delete(id: String)
}