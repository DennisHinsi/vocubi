/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import dennis.hinsi.domain.model.TrainWriteResult
import dennis.hinsi.domain.model.TrainWriteResultExtended

@Dao
interface TrainWriteResultDAO {
    @Insert
    suspend fun insert(trainWriteResult: TrainWriteResult)

    @Query("DELETE FROM train_write_result WHERE book_id = :bookId")
    suspend fun deleteAll(bookId: String)

    @Transaction
    @Query("SELECT * FROM train_write_result WHERE train_id = :trainId")
    fun loadAll(trainId: String): LiveData<List<TrainWriteResultExtended>>
}