/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import dennis.hinsi.domain.model.Voc

@Dao
interface VocDAO {
    @Insert
    suspend fun insert(Voc: Voc)

    @Query("SELECT * FROM VocContent WHERE book_id == :bookId")
    fun observe(bookId: String): LiveData<List<Voc>>

    @Query("SELECT * FROM VocContent WHERE book_id == :bookId")
    fun loadAllSingle(bookId: String): List<Voc>

    @Query("SELECT * FROM VocContent WHERE expression_lang_0 LIKE  '%' || :query || '%' OR expression_lang_1 LIKE  '%' || :query || '%'")
    fun search(query: String): LiveData<List<Voc>>

    @Query("SELECT * FROM VocContent WHERE id = :id")
    suspend fun load(id: String): Voc

    @Update
    suspend fun update(Voc: Voc)

    @Query("DELETE FROM VocContent WHERE id = :id")
    suspend fun delete(id: String)

    @Query("DELETE FROM VocContent WHERE book_id = :bookId")
    suspend fun deleteAllOfBook(bookId: String)
}