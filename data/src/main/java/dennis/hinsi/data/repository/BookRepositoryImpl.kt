/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.data.database.BookDAO
import dennis.hinsi.domain.model.Book
import dennis.hinsi.domain.model.BookExtended
import dennis.hinsi.domain.repository.BookRepository

class BookRepositoryImpl(
    private val bookDAO: BookDAO
) : BookRepository {
    override suspend fun create(book: Book) = bookDAO.insert(book)

    override fun observe(): LiveData<List<Book>> = bookDAO.observe()

    override fun observeExtended(): LiveData<List<BookExtended>> = bookDAO.observeExtended()

    override fun load(id: String) = bookDAO.load(id)

    override fun search(query: String): LiveData<List<Book>> = bookDAO.search(query)

    override suspend fun loadSingle(id: String): Book = bookDAO.loadSingle(id)

    override suspend fun update(book: Book) = bookDAO.update(book)

    override suspend fun delete(id: String) = bookDAO.delete(id)
}