/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.data.database.VocDAO
import dennis.hinsi.domain.model.Voc
import dennis.hinsi.domain.repository.VocRepository

class VocRepositoryImpl(
    private val vocDAO: VocDAO
) : VocRepository {
    override suspend fun create(voc: Voc) = vocDAO.insert(voc)

    override suspend fun delete(id: String) = vocDAO.delete(id)

    override suspend fun deleteAllOfBook(bookId: String) = vocDAO.deleteAllOfBook(bookId)

    override fun observe(bookId: String): LiveData<List<Voc>> = vocDAO.observe(bookId)

    override suspend fun loadAllSingle(bookId: String): List<Voc> = vocDAO.loadAllSingle(bookId)

    override fun search(query: String): LiveData<List<Voc>> = vocDAO.search(query)

    override suspend fun update(voc: Voc) = vocDAO.update(voc)

    override suspend fun load(id: String) = vocDAO.load(id)
}