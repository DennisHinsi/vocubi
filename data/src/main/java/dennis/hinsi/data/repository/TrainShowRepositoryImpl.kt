/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.data.database.TrainShowDAO
import dennis.hinsi.domain.model.TrainShow
import dennis.hinsi.domain.model.TrainShowExtended
import dennis.hinsi.domain.repository.TrainShowRepository

class TrainShowRepositoryImpl(
    private val trainShowDAO: TrainShowDAO
) : TrainShowRepository {
    override suspend fun insert(trainShow: TrainShow) = trainShowDAO.insert(trainShow)

    override suspend fun update(trainShow: TrainShow) = trainShowDAO.update(trainShow)

    override suspend fun delete(id: String) = trainShowDAO.delete(id)

    override suspend fun deleteAllOfBook(bookId: String) = trainShowDAO.deleteAllOfBook(bookId)

    override fun observe(): LiveData<List<TrainShowExtended>> = trainShowDAO.observe()

    override suspend fun load(id: String): TrainShow = trainShowDAO.load(id)

    override suspend fun getTrainShowExtended(
        id: String
    ): TrainShowExtended = trainShowDAO.getTrainShowExtended(id)
}