/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.data.database.TrainWriteResultDAO
import dennis.hinsi.domain.model.TrainWriteResult
import dennis.hinsi.domain.model.TrainWriteResultExtended
import dennis.hinsi.domain.repository.TrainWriteResultRepository

class TrainWriteResultRepositoryImpl(
    private val trainWriteResultDAO: TrainWriteResultDAO
) : TrainWriteResultRepository {
    override suspend fun insert(
        trainWriteResult: TrainWriteResult
    ) = trainWriteResultDAO.insert(trainWriteResult)

    override suspend fun deleteAll(bookId: String) = trainWriteResultDAO.deleteAll(bookId)

    override fun loadAll(
        trainId: String
    ): LiveData<List<TrainWriteResultExtended>> = trainWriteResultDAO.loadAll(trainId)
}