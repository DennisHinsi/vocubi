/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.data.database.TrainWriteDAO
import dennis.hinsi.domain.model.TrainWrite
import dennis.hinsi.domain.model.TrainWriteExtended
import dennis.hinsi.domain.repository.TrainWriteRepository

class TrainWriteRepositoryImpl(private val trainWriteDAO: TrainWriteDAO) : TrainWriteRepository {
    override suspend fun insert(trainWrite: TrainWrite) = trainWriteDAO.insert(trainWrite)

    override suspend fun update(trainWrite: TrainWrite) = trainWriteDAO.update(trainWrite)

    override suspend fun load(id: String): TrainWriteExtended = trainWriteDAO.load(id)

    override suspend fun delete(id: String) = trainWriteDAO.delete(id)

    override suspend fun deleteAll(bookId: String) = trainWriteDAO.deleteAll(bookId)

    override fun observe(): LiveData<List<TrainWriteExtended>> = trainWriteDAO.observe()
}