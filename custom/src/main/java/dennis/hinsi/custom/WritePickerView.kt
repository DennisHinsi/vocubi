/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.custom

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import dennis.hinsi.custom.adapter.WritePickAdapter
import dennis.hinsi.custom.adapter.WriteSelectionAdapter
import kotlinx.android.synthetic.main.view_writer_picker.view.*

class WritePickerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private interface OnCompletionListener {
        fun onComplete(isMatch: Boolean, string: String)
    }

    private var pickBackgroundTintEnabled = Color.GRAY
    private var pickForegroundTintEnabled = Color.WHITE
    private var pickBackgroundTintDisabled = Color.WHITE
    private var pickForegroundTintDisabled = Color.GRAY
    private var selectionBackgroundTintEnabled = Color.BLUE
    private var selectionForegroundTintEnabled = Color.WHITE
    private var selectionBackgroundTintDisabled = Color.BLUE
    private var selectionForegroundTintDisabled = Color.WHITE

    private var writePickAdapter: WritePickAdapter? = null
    private var writeSelectionAdapter: WriteSelectionAdapter? = null

    private var onCompletionListener: OnCompletionListener? = null

    private var letter: String = ""
    private var letterNoWhiteSpace: String = ""
    private var letterWithRandomChars = mutableListOf<Char>()
    private var randomCount: Int = 0
    private var pickAdapterRowLetterCount: Int = 6
    private var selectionAdapterRowLetterCount: Int = 8

    init {
        inflate(context, R.layout.view_writer_picker, this)

        attrs?.let {
            with(context.obtainStyledAttributes(it, R.styleable.WritePickerView)) {
                parseArguments(this)
                recycle()
            }
        }
    }

    fun setLetters(text: String, randCount: Int) {
        letter = text
        letterNoWhiteSpace = text.filter { it != ' ' }
        randomCount = randCount
        createRandomLetter()
        prepareAdapter()
        updateAdapter()
    }

    fun onComplete(isMatchBlock: (Boolean, String) -> Unit) {
        onCompletionListener = object : OnCompletionListener {
            override fun onComplete(isMatch: Boolean, string: String) {
                isMatchBlock(isMatch, string)
            }
        }
    }

    fun reset() {
        writePickAdapter?.reset()
        writeSelectionAdapter?.reset()
    }

    private fun parseArguments(typedArray: TypedArray) {
        randomCount = typedArray.getInt(R.styleable.WritePickerView_randLetterCount, randomCount)

        pickAdapterRowLetterCount = typedArray.getInt(
            R.styleable.WritePickerView_pickLetterCount,
            pickAdapterRowLetterCount
        )

        selectionAdapterRowLetterCount = typedArray.getInt(
            R.styleable.WritePickerView_selectedRowLetterCount,
            selectionAdapterRowLetterCount
        )

        pickBackgroundTintEnabled = typedArray.getColor(
            R.styleable.WritePickerView_pickLetterTintEnabled,
            pickBackgroundTintEnabled
        )

        pickForegroundTintEnabled = typedArray.getColor(
            R.styleable.WritePickerView_pickLetterTextTintEnabled,
            pickForegroundTintEnabled
        )

        pickBackgroundTintDisabled = typedArray.getColor(
            R.styleable.WritePickerView_pickLetterTintDisabled,
            pickBackgroundTintDisabled
        )

        pickForegroundTintDisabled = typedArray.getColor(
            R.styleable.WritePickerView_pickLetterTextTintDisabled,
            pickForegroundTintDisabled
        )

        selectionBackgroundTintEnabled = typedArray.getColor(
            R.styleable.WritePickerView_selectedLetterTintEnabled,
            selectionBackgroundTintEnabled
        )

        selectionForegroundTintEnabled = typedArray.getColor(
            R.styleable.WritePickerView_selectedLetterTextTintEnabled,
            selectionForegroundTintEnabled
        )

        selectionBackgroundTintDisabled = typedArray.getColor(
            R.styleable.WritePickerView_selectedLetterTintDisabled,
            selectionBackgroundTintDisabled
        )

        selectionForegroundTintDisabled = typedArray.getColor(
            R.styleable.WritePickerView_selectedLetterTextTintDisabled,
            selectionForegroundTintDisabled
        )
    }

    private fun createRandomLetter() {
        letterWithRandomChars = letterNoWhiteSpace.toMutableList()
        if (randomCount > 0) {
            for (i in 0 until randomCount) {
                letterWithRandomChars.add(letterNoWhiteSpace[(letterNoWhiteSpace.indices).random()])
            }
        }
        letterWithRandomChars = letterWithRandomChars.toMutableList().apply {
            shuffle()
        }
    }

    private fun prepareAdapter() {
        writePickAdapter = WritePickAdapter(
            pickBackgroundTintEnabled,
            pickForegroundTintEnabled,
            pickBackgroundTintDisabled,
            pickForegroundTintDisabled
        )
        pick_elements.layoutManager = GridLayoutManager(context, pickAdapterRowLetterCount)
        pick_elements.adapter = writePickAdapter

        writeSelectionAdapter = WriteSelectionAdapter(
            selectionBackgroundTintEnabled,
            selectionForegroundTintEnabled,
            selectionBackgroundTintDisabled,
            selectionForegroundTintDisabled
        )
        show_elements.layoutManager = GridLayoutManager(context, selectionAdapterRowLetterCount)
        show_elements.adapter = writeSelectionAdapter
        writeSelectionAdapter?.create(letterNoWhiteSpace.length)
    }

    private fun updateAdapter() {
        writePickAdapter?.setContent(letterWithRandomChars)
        writePickAdapter?.onClick {
            writeSelectionAdapter?.post(it.char, it.index) { selectionString ->
                writePickAdapter?.block()
                onCompletionListener?.onComplete(selectionString == letterNoWhiteSpace, selectionString)
            }
        }
    }
}
