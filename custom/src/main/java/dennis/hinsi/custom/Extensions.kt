/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.custom

internal fun tryIgnore(block: () -> Unit) {
    try {
        block()
    } catch (e: Exception) {
    }
}

internal interface OnClickListener<T> {
    fun onItemClick(t: T)
}

internal fun <K, V> hashMap(size: Int, each: (Int) -> Pair<K, V>): HashMap<K, V> = hashMapOf<K, V>().apply {
    for (i in 0 until size) {
        val pair = each(i)
        this[pair.first] = pair.second
    }
}