/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.custom.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.custom.hashMap

class WriteSelectionAdapter(
    private val letterBackgroundTintEnabled: Int,
    private val letterForegroundTintEnabled: Int,
    private val letterBackgroundTintDisabled: Int,
    private val letterForegroundTintDisabled: Int
) : RecyclerView.Adapter<WriteSelectionItemVH>() {

    private var map = hashMapOf<Int, WriteSelectionItem?>()
    private var index: Int = -1
    private var letterSize: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WriteSelectionItemVH =
        WriteSelectionItemVH.newInstance(parent)

    override fun onBindViewHolder(holder: WriteSelectionItemVH, position: Int) {
        holder.bind(
            map[position],
            letterBackgroundTintEnabled,
            letterForegroundTintEnabled,
            letterBackgroundTintDisabled,
            letterForegroundTintDisabled
        )
    }

    override fun getItemCount(): Int = map.size

    fun create(letterSize: Int) {
        this.letterSize = letterSize
        reset()
    }

    fun post(char: Char, pickIndex: Int, isComplete: (String) -> Unit) {
        if (index + 1 < map.size) {
            index++
            map[index] = WriteSelectionItem(char, pickIndex)

            if (index + 1 == map.size)
                isComplete(map.values.map { it?.char }.joinToString(""))
            notifyDataSetChanged()
        }
    }

    fun reset() {
        map = hashMap(letterSize) {
            it to null
        }
        index = -1
        notifyDataSetChanged()
    }
}



