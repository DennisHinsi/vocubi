/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.custom.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.custom.R

internal class ColorPickerItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    private val colorView = view.findViewById(R.id.item_color_view) as ImageView

    fun bind(
        item: ColorPickerItem,
        @DrawableRes selectionIcon: Int,
        onClick: (ColorPickerItem) -> Unit
    ) {
        view.setOnClickListener {
            onClick(item)
        }
        if (item.selected) {
            colorView.setImageDrawable(
                view.resources.getDrawable(
                    selectionIcon,
                    view.context.theme
                )
            )
        } else {
            colorView.setImageDrawable(null)
        }
        colorView.setBackgroundColor(item.color)
    }

    companion object {
        fun newInstance(parent: ViewGroup) = ColorPickerItemViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(
                    R.layout.item_color_picker,
                    parent,
                    false
                )
        )
    }
}
