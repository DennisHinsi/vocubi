/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.custom.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.custom.OnClickListener
import dennis.hinsi.custom.tryIgnore

class WritePickAdapter(
    private val letterBackgroundTintEnabled: Int,
    private val letterForegroundTintEnabled: Int,
    private val letterBackgroundTintDisabled: Int,
    private val letterForegroundTintDisabled: Int
) : RecyclerView.Adapter<WritePickItemVH>() {

    private var map = hashMapOf<Int, WritePickItem>()
    private var onClickListener: OnClickListener<WritePickItem>? = null
    private var isBlocked: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WritePickItemVH =
        WritePickItemVH.newInstance(parent)

    override fun onBindViewHolder(holder: WritePickItemVH, position: Int) {
        map[position]?.let { pickItem ->
            holder.bind(
                pickItem,
                letterBackgroundTintEnabled,
                letterForegroundTintEnabled,
                letterBackgroundTintDisabled,
                letterForegroundTintDisabled
            ) { selectedPickItem ->
                if (!isBlocked) {
                    tryIgnore {
                        map[position]!!.isEnabled = !map[position]!!.isEnabled
                        notifyDataSetChanged()
                    }
                    onClickListener?.onItemClick(selectedPickItem)
                }
            }
        }
    }

    override fun getItemCount(): Int = map.size

    fun setContent(letter: MutableList<Char>) {
        letter.forEachIndexed { index, char ->
            map[index] = WritePickItem(char, index, true)
        }
        notifyDataSetChanged()
    }

    fun reset() {
        map.values.map { it.isEnabled = true }
        isBlocked = false
        notifyDataSetChanged()
    }

    fun onClick(block: (WritePickItem) -> Unit) {
        onClickListener = object : OnClickListener<WritePickItem> {
            override fun onItemClick(t: WritePickItem) {
                block(t)
            }
        }
    }

    fun block() {
        isBlocked = true
    }
}


