/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.custom.adapter

import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView

internal class ColorPickerAdapter(
    @DrawableRes private val selectionIcon: Int,
    private val onClick: (ColorPickerItem) -> Unit
) : RecyclerView.Adapter<ColorPickerItemViewHolder>() {

    private var list = mutableListOf<ColorPickerItem>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ColorPickerItemViewHolder = ColorPickerItemViewHolder.newInstance(parent)

    override fun onBindViewHolder(holder: ColorPickerItemViewHolder, position: Int) {
        holder.bind(list[position], selectionIcon) {
            onClick(it)
            list.mapIndexed { index, each ->
                each.selected = index == position
            }
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int = list.size

    fun setColors(color: List<ColorPickerItem>) {
        list.clear()
        list.addAll(color)
        notifyDataSetChanged()
    }

    fun selectColor(color: Int) {
        list.map { each ->
            each.selected = each.color == color
        }
        notifyDataSetChanged()
    }
}