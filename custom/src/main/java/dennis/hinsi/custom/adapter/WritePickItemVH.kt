/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.custom.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.custom.R

class WritePickItemVH(private val view: View) : RecyclerView.ViewHolder(view) {

    private val textView = view.findViewById(R.id.item_pick_text) as TextView

    fun bind(
        itemWrite: WritePickItem,
        letterBackgroundTintEnabled: Int,
        letterForegroundTintEnabled: Int,
        letterBackgroundTintDisabled: Int,
        letterForegroundTintDisabled: Int,
        onClick: (WritePickItem) -> Unit
    ) {
        if (itemWrite.isEnabled) {
            textView.backgroundTintList = ColorStateList.valueOf(letterBackgroundTintEnabled)
            textView.setTextColor(letterForegroundTintEnabled)
        } else {
            textView.backgroundTintList = ColorStateList.valueOf(letterBackgroundTintDisabled)
            textView.setTextColor(letterForegroundTintDisabled)
        }

        view.setOnClickListener {
            onClick(itemWrite)
        }
        textView.text = itemWrite.char.toString()
    }

    companion object {
        fun newInstance(parent: ViewGroup) = WritePickItemVH(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_writer_picker_pick, parent, false)
        )
    }
}