/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.custom

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import dennis.hinsi.custom.adapter.ColorPickerAdapter
import dennis.hinsi.custom.adapter.ColorPickerItem
import kotlinx.android.synthetic.main.view_color_picker.view.*

class ColorPickerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var rowCount = 4
    private var selectionIcon: Int = R.drawable.ic_selected_white_24
    private var adapter: ColorPickerAdapter? = null

    var selectedColor: Int = Color.BLACK
        private set

    init {
        inflate(context, R.layout.view_color_picker, this)

        attrs?.let {
            with(context.obtainStyledAttributes(it, R.styleable.ColorPickerView)) {
                val title = getString(R.styleable.ColorPickerView_title)
                if (title != null)
                    color_picker_title.text = title
                else
                    color_picker_title.visibility = View.GONE

                rowCount = getInt(R.styleable.ColorPickerView_row_count, rowCount)
                selectionIcon =
                    getResourceId(R.styleable.ColorPickerView_selection_icon, selectionIcon)
                recycle()
            }
        }
    }

    fun setColors(color: List<Int>) {
        if (color.isEmpty())
            throw Exception("COLORS CANNOT BE EMPTY")
        selectedColor = color[0]
        adapter = ColorPickerAdapter(selectionIcon) {
            selectedColor = it.color
        }
        color_picker_content.layoutManager = GridLayoutManager(context, rowCount)
        color_picker_content.adapter = adapter
        adapter?.setColors(color.mapIndexed { index, each ->
            ColorPickerItem(each, index == 0)
        })
    }

    fun setSelectedColor(color: Int) {
        selectedColor = color
        adapter?.selectColor(color)
    }
}

